#!/bin/bash

# Slackware build script for kmod

# Copyright 2011-2016  Robby Workman, Tuscaloosa, Alabama, USA
# Copyright 2013-2025  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0) ; CWD=$(pwd)

PKGNAM=kmod
VERSION=${VERSION:-$(echo $PKGNAM-*.tar.?z | rev | cut -f 3- -d . | cut -f 1 -d - | rev)}
BUILD=${BUILD:-1}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i686 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

# If the variable PRINT_PACKAGE_NAME is set, then this script will report what
# the name of the created package would be, and then exit. This information
# could be useful to other scripts.
if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PKGNAM-$VERSION-$ARCH-$BUILD.txz"
  exit 0
fi

NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}

TMP=${TMP:-/tmp}
PKG=$TMP/package-$PKGNAM

if [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=pentium4 -mtune=generic"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -march=x86-64 -mtune=generic -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PKGNAM-$VERSION
tar xvf $CWD/$PKGNAM-$VERSION.tar.?z || exit 1
cd $PKGNAM-$VERSION || exit 1
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \+ -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \+

# Configure, build, and install:
export CFLAGS="$SLKCFLAGS"
export CXXFLAGS="$SLKCFLAGS"
mkdir meson-build
cd meson-build
meson setup \
  --prefix=/usr \
  --libdir=/lib${LIBDIRSUFFIX} \
  --libexecdir=/usr/libexec \
  --bindir=/sbin \
  --sbindir=/sbin \
  --includedir=/usr/include \
  --datadir=/usr/share \
  --mandir=/usr/man \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --buildtype=release \
  -Dopenssl=enabled \
  -Dxz=enabled \
  -Dzlib=enabled \
  -Dzstd=enabled \
  .. || exit 1
  "${NINJA:=ninja}" $NUMJOBS || exit 1
  DESTDIR=$PKG $NINJA install || exit 1
cd ..

# Fix paths in manual page:
sed -i 's?/usr/lib/`uname -r`?/lib/modules/`uname -r`?g' $PKG/usr/man/man8/modprobe.8 || exit 1

# Strip binaries:
( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
)

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Link lsmod into /bin
mkdir -p $PKG/bin
( cd $PKG/bin ; ln -sf /sbin/lsmod . )

mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}
mv $PKG/lib${LIBDIRSUFFIX}/pkgconfig $PKG/usr/lib${LIBDIRSUFFIX}
rm -f $PKG/lib${LIBDIRSUFFIX}/{*.so}
( cd $PKG/usr/lib${LIBDIRSUFFIX}
  ln -sf ../../lib${LIBDIRSUFFIX}/libkmod.so.? libkmod.so
)

mkdir -p $PKG/usr/doc/$PKGNAM-$VERSION
cp -a \
  CODING-STYLE* CONTRIBUTING* COPYING* DCO* NEWS* README* \
  $PKG/usr/doc/$PKGNAM-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PKGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $TMP/$PKGNAM-$VERSION-$ARCH-$BUILD.txz
