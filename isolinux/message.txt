
Welcome to 09Slackware6407 version 15.1 (Linux kernel 6.12.18)!

If you need to pass extra parameters to the kernel, enter them at the prompt
below after the name of the kernel to boot (e.g., generic.s).

In a pinch, you can boot your system from here with a command like:

boot: generic.s root=/dev/sda1 initrd= ro 

In the example above, /dev/sda1 is the / Linux partition.

To test your memory with memtest86+, enter memtest on the boot line below.

This prompt is just for entering extra parameters. If you don't need to enter
any parameters, hit ENTER to boot the default kernel "generic.s" or press [F2]
for a listing of more kernel choices. Default kernel will boot in 2 minutes.

